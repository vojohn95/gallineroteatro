<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Form;
use App\Models\Cartelera;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Select;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\ImageColumn;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\TimePicker;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\CarteleraResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\CarteleraResource\RelationManagers;

class CarteleraResource extends Resource
{
    protected static ?string $model = Cartelera::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('titulo')->filled(),
				TextInput::make('descripcion'),
                FileUpload::make('imagen')->filled(),
                DatePicker::make('fecha_inicio')->filled(),
                DatePicker::make('fecha_fin')->filled(),
                TimePicker::make('hora_inicio')->filled(),
                TimePicker::make('hora_fin')->filled(),
                Select::make('estado')->options([
                    'activo' => 'Activo',
                    'inactivo' => 'Inactivo',
                ])->filled(),
                TextInput::make('ubicacion')->filled(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('titulo'),
                TextColumn::make('descripcion'),
                ImageColumn::make('imagen')->width(200)->square(),
                TextColumn::make('fecha_inicio'),
                TextColumn::make('fecha_fin'),
                TextColumn::make('hora_inicio'),
                TextColumn::make('hora_fin'),
                TextColumn::make('estado'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCarteleras::route('/'),
            'create' => Pages\CreateCartelera::route('/create'),
            'edit' => Pages\EditCartelera::route('/{record}/edit'),
        ];
    }
}
