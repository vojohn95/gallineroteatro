<?php

namespace App\Filament\Resources\CarteleraResource\Pages;

use App\Filament\Resources\CarteleraResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCartelera extends CreateRecord
{
    protected static string $resource = CarteleraResource::class;
}
