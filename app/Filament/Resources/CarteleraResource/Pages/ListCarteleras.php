<?php

namespace App\Filament\Resources\CarteleraResource\Pages;

use App\Filament\Resources\CarteleraResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListCarteleras extends ListRecords
{
    protected static string $resource = CarteleraResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
