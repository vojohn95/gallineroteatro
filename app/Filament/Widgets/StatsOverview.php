<?php

namespace App\Filament\Widgets;

use App\Models\Cartelera;
use App\Models\Contacto;
use App\Models\User;
use Filament\Widgets\StatsOverviewWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;

class StatsOverview extends BaseWidget
{
    protected function getStats(): array
    {
        return [
            Stat::make(
                'Total contactos',
                Contacto::count(),
            )
            ->description('Total de contactos registrados')
            ->icon('heroicon-o-users')
            ->color('success'),
            Stat::make(
                'Total eventos',
                Cartelera::count(),
            )
            ->description('Total de carteleras registrados')
            ->icon('heroicon-o-calendar')
            ->color('success'),
            Stat::make(
                'Total usuarios',
                User::count(),
            )
            ->description('Total de usuarios registrados')
            ->icon('heroicon-o-user-group')
            ->color('success'),
        ];
    }
}
