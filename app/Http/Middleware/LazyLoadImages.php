<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LazyLoadImages
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var \Illuminate\Http\Response $response */
        $response = $next($request);

        // Only modify content if the response is HTML
        if ($response instanceof Response && $response->headers->get('Content-Type') && str_contains($response->headers->get('Content-Type'), 'text/html')) {
            $content = $response->getContent();
            $content = $this->addLazyLoadingToImages($content);
            $response->setContent($content);
        }

        return $response;
    }

    /**
     * Add loading="lazy" to all <img> tags.
     *
     * @param  string  $content
     * @return string
     */
    protected function addLazyLoadingToImages($content)
    {
        return preg_replace('/<img(?!.*loading=)/', '<img loading="lazy"', $content);
    }
}
