<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Contacto;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class ContactoComponent extends Component
{
    use LivewireAlert;

    public $nombre;
    public $edad;
    public $email;
    public $institucion;
    public $telefono;
    public $asunto;
    public $mensaje;


    public function render()
    {
        return view('livewire.contacto-component');
    }

    public function submit()
    {
        Contacto::create([
            'nombre' => $this->nombre,
            'edad' => $this->edad,
            'email' => $this->email,
            'institucion' => $this->institucion,
            'telefono' => $this->telefono,
            'asunto' => $this->asunto,
            'mensaje' => $this->mensaje,
        ]);

        $this->reset('nombre', 'edad', 'email', 'institucion', 'telefono', 'asunto', 'mensaje');

        $this->alert('success', '¡Mensaje enviado!', [
            'position' => 'center',
            'timer' => 3000,
            'toast' => true,
            'showConfirmButton' => false,
            'onConfirmed' => '',
            'timerProgressBar' => true,
           ]);
    }
}
