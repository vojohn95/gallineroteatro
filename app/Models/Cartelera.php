<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cartelera
 *
 * @property int $id
 * @property string $titulo
 * @property string|null $descripcion
 * @property string|null $imagen
 * @property Carbon $fecha_inicio
 * @property Carbon $fecha_fin
 * @property string $hora_inicio
 * @property string $hora_fin
 * @property string $estado
 * @property longtext $ubicacion
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Cartelera extends Model
{
	protected $table = 'carteleras';

	protected $casts = [
		'fecha_inicio' => 'datetime',
		'fecha_fin' => 'datetime'
	];

	protected $fillable = [
		'titulo',
		'descripcion',
		'imagen',
		'fecha_inicio',
		'fecha_fin',
		'hora_inicio',
		'hora_fin',
		'estado',
		'ubicacion'
	];
}
