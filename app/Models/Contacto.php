<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contacto
 *
 * @property int $id
 * @property string $nombre
 * @property string $edad
 * @property string $email
 * @property string $institucion
 * @property string|null $telefono
 * @property string $asunto
 * @property string $mensaje
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @package App\Models
 */
class Contacto extends Model
{
	use SoftDeletes;
	protected $table = 'contactos';

	protected $fillable = [
		'nombre',
        'edad',
		'email',
		'institucion',
		'telefono',
		'asunto',
		'mensaje'
	];
}
