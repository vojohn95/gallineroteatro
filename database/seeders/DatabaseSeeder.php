<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Claudia Cervantes',
            'email' => 'clauscerman@gmail.com',
            'password' => bcrypt('Password7'),
        ]);
    }
}


//luciapardotl@gmail.com
//Password1

//gallinero.contacto@gmail.com
//Password3

//jesicabastidas@gmail.com
//Password5

//clauscerman@gmail.com
//Password7
