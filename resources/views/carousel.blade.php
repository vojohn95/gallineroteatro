<style>
    .carousel {
        position: relative;
    }

    .wave-svg {
        position: absolute;
        z-index: 10;
        bottom: 0;
        left: 0;
        right: 0;
        top: 100%;
    }

    /* Media queries */
    @media screen and (max-width: 992px) {
        .wave-svg {
            top: 55%;
            /* Ajusta la posición en pantallas medianas */
        }
    }

    @media screen and (max-width: 768px) {
        .wave-svg {
            top: 70%;
            /* Ajusta la posición en pantallas pequeñas */
        }
    }

    @media screen and (min-width: 350px) {
        .wave-svg {
            top: 70%;
            /* Ajusta la posición en pantallas pequeñas */
        }
    }

    @media screen and (min-width: 1200px) {
        .wave-svg {
            top: 70%;
            /* Ajusta la posición en pantallas grandes */
        }
    }

    @media screen and (min-width: 1600px) {
        .wave-svg {
            top: 70%;
            /* Ajusta la posición en pantallas grandes */
        }
    }

    .frase-larga-carousel {
        padding-left: 41%;
        font-size: 22px;
        font-style: italic;
    }

    .margin-top-autor {
        margin-top: 0%;
    }

    .carousel-indicators {
        display: none;
    }
</style>
<!-- Carousel wrapper -->
<div id="carouselMaterialStyle" class="carousel slide carousel-fade" data-mdb-ride="carousel" data-mdb-carousel-init>
    <!-- Indicators -->
    <div class="carousel-indicators">
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="0" class="active"
            aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="1"
            aria-label="Slide 2"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="2"
            aria-label="Slide 3"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="3"
            aria-label="Slide 4"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="4"
            aria-label="Slide 5"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="5"
            aria-label="Slide 6"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="6"
            aria-label="Slide 7"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="7"
            aria-label="Slide 8"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="8"
            aria-label="Slide 9"></button>
        <button type="button" data-mdb-target="#carouselMaterialStyle" data-mdb-slide-to="9"
            aria-label="Slide 10"></button>
    </div>

    <style>
        @media screen and (max-width: 480px) {
            .span-carousel {
                padding-left: 0;
                font-size: 12px !important;
                /* Agregado !important para dar prioridad */
            }

            .h2-carousel {
                font-size: 18px;
                font-style: italic;
            }
        }

        @media screen and (max-width: 480px) {
            .span-carousel-largo {
                font-size: 12px !important;
            }

        }

        @media screen and (min-width: 900px) and (max-width: 4000px) {
            .span-carousel {
                padding-left: 66%;
                font-size: 22px;
                font-style: italic;
                margin-bottom: -50%;
            }
        }

        @media screen and (min-width: 900px) and (max-width: 4000px) {
            .span-carousel-largo {
                padding-left: 41%;
                font-size: 22px;
                font-style: italic;
                margin-bottom: -50%;
            }
        }
    </style>

    <!-- Inner -->
    <div class="carousel-inner rounded-5 shadow-4-strong">
        <!-- Single item -->
        <div class="carousel-item active">
            <img loading="lazy" src="{{ asset('assets/carousel/1.Machete-Foto Luis Ekiro.webp') }}"
                class="d-block w-100 img-fluid margin-top-autor" style="height: 80%;" alt="Sunset Over the City" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">"La risa es una cosa muy seria."</strong>
                        <span class="span-carousel">Groucho Marx</span>
                    </h2>
                </div>
            </div>
        </div>

        <!-- Single item -->
        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/9.ElOtro-JesicaBastidas.Foto_ aarón Govea.webp') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">“Él puede parecer un idiota y actuar
                            como un idiota,
                            pero no se deje engañar usted, es realmente un idiota”.</strong>
                        <span class="span-carousel">Groucho Marx</span>
                    </h2>
                </div>
            </div>
        </div>

        <!-- Single item -->
        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/Buenos Dias somos Sus Guias 3 Septiembte-1541.webp') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">“En nada se revela mejor el carácter de las personas como en los
                            motivos de su risa”.</strong>
                        <span class="span-carousel">Goethe</span>
                    </h2>
                </div>
            </div>
        </div>

        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/4.Loving You Foto Carlos Miravá.png') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">“La sátira es el arma más eficaz contra el poder”.
                        </strong>
                        <span class="span-carousel"> Darío Fo </span>
                    </h2>
                </div>
            </div>
        </div>

        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/8. El huevo Enid .png') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">"La cosa más simple, ejecutada demasiado rápido o demasiado lento,
                            puede tener los efectos más desastrosos".
                        </strong>
                        <span class="span-carousel"> Buster Keaton
                        </span>
                    </h2>
                </div>
            </div>
        </div>

        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/ElOtro-JesicaBastidas.webp') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">
                            “El payaso es un oficio, pero también un estado del espíritu”.
                        </strong>
                        <span class="span-carousel"> Annie Fratellini
                        </span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/Muleca Fea-Lucía Pardo. Foto Carlos Miravá. Dirección Nohemí Espinosa..webp') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">
                            “Ser complicado es simple, ser simple es complicado”.
                        </strong>
                        <span class="span-carousel-largo"> Gardi Hutter, Avner el Excéntrico y un músico.</span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/2.Piano-Lucía Pardo. Foto Carlos Miravá. Dirección Nohemí Espinosa.webp') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">
                            “La cuarta pared en el payaso está detrás del público”.
                        </strong>
                        <span class="span-carousel"> Claret Clown
                        </span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/6.Virgen-JesicaBastidas.Foto_EnidHernández.webp') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">
                            “Recuerda siempre que puedes agacharte y no recoger nada”.
                        </strong>
                        <span class="span-carousel"> Charles Chaplin
                        </span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img loading="lazy" src="{{ asset('assets/carousel/10.TR3SLUSTR3S Foto Pili Pala.webp') }}"
                class="d-block w-100 img-fluid margin-top-autor" alt="Sunset Over the City" style="height: 80%;" />
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <h2 class="mb-0 text-white" style="white-space: pre-line; text-align: center;">
                        <strong class="h2-carousel">
                            “...tan tán”.
                        </strong>
                        <span class="span-carousel-largo"> El Fin
                        </span>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" class="wave-svg">
        <path fill="white" fill-opacity="1"
            d="M0,64L48,106.7C96,149,192,235,288,261.3C384,288,480,256,576,218.7C672,181,768,139,864,106.7C960,75,1056,53,1152,58.7C1248,64,1344,96,1392,112L1440,128L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z">
        </path>
    </svg>

    <!-- Inner -->

    <!-- Controls -->
    <button class="carousel-control-prev" type="button" data-mdb-target="#carouselMaterialStyle"
        data-mdb-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-mdb-target="#carouselMaterialStyle"
        data-mdb-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
<!-- Carousel wrapper -->
