@extends('layouts.app')
@section('hero')
    <style>
        .hero-section {
            /* background: url('assets/cartelera/DSC01327.jpg'); */
            background: linear-gradient(to right, rgba(1, 173, 182, 0.4) 0%, rgba(1, 173, 182, 0.4) 100%), url('assets/cartelera/DSC01327.jpg');
            position: relative;
            height: 80vh;
            width: 100vw;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
        }

        .hero-section .wave {
            width: 100%;
            overflow: hidden;
            position: absolute;
            z-index: 1;
            bottom: -50px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .wave {
                bottom: -180px;
            }
        }

        .hero-section .wave svg {
            width: 100%;
        }

        .hero-section,
        .hero-section>.container>.row {
            height: 100vh;
            min-height: 880px;
        }

        .hero-section.inner-page {
            height: 60vh;
            min-height: 0;
        }

        .hero-section.inner-page .hero-text {
            transform: translateY(-150px);
            margin-top: -120px;
        }

        @media screen and (max-width: 992px) {
            .hero-section.inner-page .hero-text {
                margin-top: -80px;
            }
        }

        .hero-section h1 {
            font-size: 3.5rem;
            color: #fff;
            font-weight: 700;
            margin-bottom: 30px;
        }

        @media screen and (max-width: 992px) {
            .hero-section h1 {
                font-size: 2.5rem;
                text-align: center;
                margin-top: 40px;
            }
        }

        @media screen and (max-width: 992px) {
            .hero-section .hero-text-image {
                margin-top: 4rem;
            }
        }

        .hero-section p {
            font-size: 18px;
            color: #fff;
        }

        .hero-section .iphone-wrap {
            position: relative;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap {
                text-align: center;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            position: absolute;
            top: -50%;
            overflow: hidden;
            left: 0;
            /* box-shadow: 0 15px 50px 0 rgba(0, 0, 0, 0.3); */
            border-radius: 30px;
        }

        @media screen and (max-width: 992px) {

            .hero-section .iphone-wrap .phone-2,
            .hero-section .iphone-wrap .phone-1 {
                position: relative;
                top: 0;
                max-width: 100%;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-1 {
                margin-left: -150px;
            }
        }

        .hero-section .iphone-wrap .phone-2 {
            margin-top: 50px;
            margin-left: 100px;
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-2 {
                width: 250px;
                position: absolute;
                margin-top: 0px;
                margin-left: 100px;
            }
        }

        .egg-image {
            width: 275px;
            height: 335px;
            border-radius: 50% 50% 50% 50% / 60% 60% 40% 40%;
        }

        .img-thumbnail {
            transition: transform 0.2s ease-in-out;
        }

        .img-thumbnail:hover {
            transform: scale(1.1);
        }

        @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,700,800');

        * {
            box-sizing: border-box;
            margin: 0;
        }

        html,
        body {
            margin: 0;
            background: white;
            font-family: 'Montserrat', helvetica, arial, sans-serif;
            font-size: 14px;
            font-weight: 400;
        }

        .link {
            display: block;
            text-align: center;
            color: #777;
            text-decoration: none;
            padding: 10px;
        }

        .movie_card {
            position: relative;
            display: block;
            width: 900px;
            height: 450px;
            margin: 90px auto;
            overflow: hidden;
            border-radius: 10px;
            transition: all 0.4s;
            box-shadow: 0px 0px 120px -25px rgba(0, 0, 0, 0.5);

            &:hover {
                transform: scale(1.02);
                box-shadow: 0px 0px 80px -25px rgba(0, 0, 0, 0.5);
                transition: all 0.4s;
            }

            .info_section {
                position: relative;
                width: 100%;
                height: 100%;
                background-blend-mode: multiply;
                z-index: 2;
                border-radius: 10px;

                .movie_header {
                    position: relative;
                    padding: 25px;
                    height: 40%;
                    overflow: hidden;
                    /* Asegura que nada se desborde */


                    h1 {
                        color: black;
                        font-weight: 400;
                    }

                    h4 {
                        color: #555;
                        font-weight: 400;
                    }

                    .minutes {
                        display: inline-block;
                        margin-top: 15px;
                        color: #555;
                        padding: 5px;
                        border-radius: 5px;
                        border: 1px solid rgba(0, 0, 0, 0.05);
                    }

                    .type {
                        display: inline-block;
                        color: #959595;
                        margin-left: 10px;
                    }

                    .locandina {
                        position: relative;
                        float: left;
                        margin-right: 20px;
                        height: 120px;
                        box-shadow: 0 0 20px -10px rgba(0, 0, 0, 0.5);
                    }
                }

                .movie_desc {
                    padding: 25px;
                    height: 50%;

                    .text {
                        color: #545454;
                    }
                }

                .movie_social {
                    height: 10%;
                    padding-left: 15px;
                    padding-bottom: 20px;

                    ul {
                        list-style: none;
                        padding: 0;

                        li {
                            display: inline-block;
                            color: rgba(0, 0, 0, 0.3);
                            transition: color 0.3s;
                            transition-delay: 0.15s;
                            margin: 0 10px;

                            &:hover {
                                transition: color 0.3s;
                                color: rgba(0, 0, 0, 0.7);
                            }

                            i {
                                font-size: 19px;
                                cursor: pointer;
                            }
                        }
                    }
                }
            }

            .blur_back {
                position: absolute;
                top: 0;
                right: 0;
                z-index: 1;
                width: 250%;
                height: 100%;
                background-size: 100% 100%;
                background-position: center;
                background-repeat: no-repeat;
                border-radius: 11px;

            }
        }

        @media screen and (min-width: 768px) {
            .movie_header {
                width: 65%;
            }

            .movie_desc {
                width: 50%;
            }

            .info_section {
                background: linear-gradient(to right, #0baaaa 4%, transparent 100%);
            }

            .blur_back {
                width: 80%;
                background-position: -100% 10% !important;
            }
        }

        @media screen and (max-width: 768px) {
            .movie_card {
                width: 95%;
                margin: 70px auto;
                min-height: 350px;
                height: auto;
            }

            .blur_back {
                width: 100%;
                background-position: 50% 50% !important;
            }

            .movie_header {
                width: 100%;
                margin-top: 85px;
            }

            .movie_desc {
                width: 100%;
            }

            .info_section {
                background: linear-gradient(to top, #0baaaa 80%, transparent 100%);
                display: inline-grid;
            }
        }

        /* Estilos para el icono normal */
        .icon {
            font-size: 24px;
            /* Tamaño del icono */
        }

        /* Estilos para el icono al pasar el mouse */
        .icon:hover {
            cursor: pointer;
            /* Cambia el cursor al pasar el mouse */
            color: rgb(0, 0, 0);
            /* Cambia el color del icono */
        }

        /* Cambia el icono al pasar el mouse */
        .icon:hover i {
            /* Puedes cambiar a cualquier otro icono de FontAwesome */
            content: "bi bi-egg-fried";
            /* Ícono de estrella rellena */
        }
    </style>

    <style>
        .image-zoom {
            transition: transform 0.3s ease;
            /* Suaviza la transición */
        }

        .image-zoom:hover {
            transform: scale(0.8);
            /* Aleja la imagen al 80% de su tamaño original */
        }
    </style>
@endsection
@section('cabecera')
    @include('layouts.cabecera', [
        'title' => '',
        'subtitle' => '',
    ])
@endsection
@section('content')
    <!-- ======= Home Section ======= -->
    @php
        use Illuminate\Support\Facades\Storage;
    @endphp
    <section class="section" style="margin-top: -50px;">
        <div class="container">
            <h2 class="text-center section-heading rojo-gallinero">Cartelera</h2>

            @foreach ($carteleras as $cartelera)
                <div class="movie_card" id="ave">
                    <div class="info_section">
                        <div class="movie_header">
                            @php
                                $fileUrl = Storage::url($cartelera->imagen);
                                if ($fileUrl == '/storage/') {
                                    $fileUrl =
                                        'https://3.bp.blogspot.com/-d_uRRX2-BwM/Wl3UDBW7uGI/AAAAAAAABng/irfEs0ITdV8Ej_YJPLbmZf08hkmYV0gogCLcBGAs/s1600/teatro.jpg';
                                }
                            @endphp
                            <h1 class="text-white text-outline">{{ ucfirst($cartelera->titulo) }}</h1>
                            <h5 class="text-white text-outline">
                                Fechas:<br> {{ date('Y/m/d', strtotime($cartelera->fecha_inicio)) }} -
                                {{ date('Y/m/d', strtotime($cartelera->fecha_fin)) }}
                            </h5>
                            <span class="text-white minutes">Horarios:</span>
                            <p class="text-white type">{{ $cartelera->hora_inicio }} - {{ $cartelera->hora_fin }}</p>
                        </div>
                        <div class="movie_desc">
                            <p class="text-white text fs-6">
                                @php
                                    if (strlen($cartelera->descripcion) > 300) {
                                        $textoCortado = substr($cartelera->descripcion, 0, 300) . '...';
                                    } else {
                                        $textoCortado = $cartelera->descripcion;
                                    }
                                @endphp
                                {{ $textoCortado }}
                            </p>
                        </div>
                        <div class="movie_social">
                            <ul>
                                <li class="text-white parpadeo">
                                    <a href="{{ $cartelera->ubicacion }}" target="_blank">
                                        <span class="text-white icon" id="icon{{ $cartelera->id }}">
                                            <i class="bi bi-egg" id="eggIcon{{ $cartelera->id }}"></i>Ver Ubicación
                                        </span>
                                    </a>
                                </li>
                                <li class="text-white parpadeo">
                                    <span class="text-white icon" data-bs-toggle="modal"
                                        data-bs-target="#imageModal{{ $cartelera->id }}">
                                        <i class="bi bi-egg" id="eggIcon{{ $cartelera->id }}"></i> Ver Imagen
                                    </span>
                                </li>
                            </ul>
                            <br>
                        </div>
                    </div>
                    <div class="blur_back image-zoom" style="background: url('{{ $fileUrl }}');"></div>
                </div>
            @endforeach
        </div>
    </section>
@endsection

@foreach ($carteleras as $cartelera)
    @php
        $fileUrl = Storage::url($cartelera->imagen);
        if ($fileUrl == '/storage/') {
            $fileUrl =
                'https://3.bp.blogspot.com/-d_uRRX2-BwM/Wl3UDBW7uGI/AAAAAAAABng/irfEs0ITdV8Ej_YJPLbmZf08hkmYV0gogCLcBGAs/s1600/teatro.jpg';
        }
    @endphp
    <!-- Modal -->
    <div class="modal fade" id="imageModal{{ $cartelera->id }}" tabindex="-1"
        aria-labelledby="modalLabel{{ $cartelera->id }}" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel{{ $cartelera->id }}">{{ $cartelera->titulo }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="{{ $fileUrl }}" alt="Imagen de {{ $cartelera->titulo }}" class="img-fluid">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endforeach

@section('scripts')
    @parent
    <!-- Asegúrate de que Bootstrap JS esté cargado -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
@endsection
