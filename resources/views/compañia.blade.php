@extends('layouts.app')
@section('hero')
    <style>
        .hero-section {
            /* background: linear-gradient(to right, rgba(0, 58, 34, 0.8) 0%, rgba(2, 78, 78, 0.8) 100%), url('assets/cabecera/IMG_8498.HEIC'); */
            background: linear-gradient(to right, rgba(1, 173, 182, 0.7) 0%, rgba(1, 173, 182, 0.7) 100%), url('assets/cabecera/IMG_8498.HEIC');
            position: relative;
            height: 80vh;
            width: 100vw;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
        }

        .hero-section .wave {
            width: 100%;
            overflow: hidden;
            position: absolute;
            z-index: 1;
            bottom: -50px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .wave {
                bottom: -180px;
            }
        }

        .hero-section .wave svg {
            width: 100%;
        }

        .hero-section,
        .hero-section>.container>.row {
            height: 100vh;
            min-height: 880px;
        }

        .hero-section.inner-page {
            height: 60vh;
            min-height: 0;
        }

        .hero-section.inner-page .hero-text {
            transform: translateY(-150px);
            margin-top: -120px;
        }

        @media screen and (max-width: 992px) {
            .hero-section.inner-page .hero-text {
                margin-top: -80px;
            }
        }

        .hero-section h1 {
            font-size: 3.5rem;
            color: #fff;
            font-weight: 700;
            margin-bottom: 30px;
        }

        @media screen and (max-width: 992px) {
            .hero-section h1 {
                font-size: 2.5rem;
                text-align: center;
                margin-top: 40px;
            }
        }

        @media screen and (max-width: 992px) {
            .hero-section .hero-text-image {
                margin-top: 4rem;
            }
        }

        .hero-section p {
            font-size: 18px;
            color: #fff;
        }

        .hero-section .iphone-wrap {
            position: relative;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap {
                text-align: center;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            position: absolute;
            top: -50%;
            overflow: hidden;
            left: 0;
            /* box-shadow: 0 15px 50px 0 rgba(0, 0, 0, 0.3); */
            border-radius: 30px;
        }

        @media screen and (max-width: 992px) {

            .hero-section .iphone-wrap .phone-2,
            .hero-section .iphone-wrap .phone-1 {
                position: relative;
                top: 0;
                max-width: 100%;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-1 {
                margin-left: -150px;
            }
        }

        .hero-section .iphone-wrap .phone-2 {
            margin-top: 50px;
            margin-left: 100px;
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-2 {
                width: 250px;
                position: absolute;
                margin-top: 0px;
                margin-left: 100px;
            }
        }

        .egg-image {
            width: 275px;
            height: 335px;
            border-radius: 50% 50% 50% 50% / 60% 60% 40% 40%;
        }

        .img-thumbnail {
            transition: transform 0.2s ease-in-out;
        }

        .img-thumbnail:hover {
            transform: scale(1.1);
        }

        .zoom-container {
            overflow: hidden;
        }
    </style>
@endsection
@section('cabecera')
    @include('layouts.cabecera', [
        'title' => '',
        'subtitle' => '',
    ])
@endsection
@section('content')
    <!-- ======= Home Section ======= -->
    <section class="section" style="margin-top: -50px;">
        <div class="container">
            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-12" data-aos="fade-up">
                    <h2 class="section-heading amarillo-gallinero">EL GALLINERO</h2>
                    <h5 style="color: gray;">Tres mujeres lideran y dirigen El Gallinero Teatro Clown, compañia mexicana de
                        artes
                        escénicas, comprometida con la creación
                        de espectáculos originales, la investigación y la enseñanza del humor a partir de combinar el clown,
                        la cienciay comedia física.
                    </h5>
                </div>
            </div>

            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-12" data-aos="fade-up">
                    <h2 class="section-heading verde-gallinero" style="color: #01adb6 !important;">OBJETIVOS</h2>
                    <h5 style="color: gray;">Generar interés y experiencias de aprendizaje gozoso en las áreas de ciencia y
                        arte; fortalecer la presencia de la mujer como protagonista
                        en dichos ámbitos, y forjar alianzas para la creación y circulación de proyectos
                        interdisciplinarios.
                    </h5>
                </div>
            </div>

            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-12" data-aos="fade-up">
                    <h2 class="section-heading rojo-gallinero">NUESTROS VALORES</h2>
                    <h5 style="color: gray;">Búsqueda del buen humor con calidad, empatía, creatividad y responsabilidad.
                    </h5>
                </div>
            </div>

            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-12" data-aos="fade-up">
                    <h2 class="section-heading amarillo-gallinero">LAS GALLINAS</h2>
                    <h5 style="color: gray;">Directoras, fundadoras y actrices de El Gallinero Teatro Clown.
                    </h5>
                </div>
            </div>

            <div class="row">
                @include('shapes.egg', [
                    'name' => 'Claudia Ivonne Cervantes',
                    'imagen' => 'assets/compañia/_MG_0329.webp',
                ])

                @include('shapes.egg', [
                    'name' => 'Jesica Bastidas',
                    'imagen' => 'assets/compañia/JesicaBastidas_2022.webp',
                ])

                @include('shapes.egg', [
                    'name' => 'Lucía Pardo Ríos',
                    'imagen' => 'assets/compañia/Fotografía 2021.webp',
                ])

            </div>

            <br><br><br>

            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-12" data-aos="fade-up">
                    <h2 class="section-heading verde-gallinero">NUESTROS GALLOS</h2>
                </div>
            </div>

            <div class="row">
                @include('shapes.egg', [
                    'name' => 'Azucena Galicia',
                    'imagen' => 'assets/compañia/Azucena.webp',
                    'description' => 'Diseñadora escénica',
                ])

                @include('shapes.egg', [
                    'name' => 'Fores Basura',
                    'imagen' => 'assets/compañia/4.webp',
                    'description' => 'Compositor musical y músico en escena',
                ])

                @include('shapes.egg', [
                    'name' => 'Bobby Watson',
                    'imagen' => 'assets/compañia/DSC_7307.jpg',
                    'description' => 'Diseño de iluminación',
                ])

                @include('shapes.egg', [
                    'name' => 'Diego Santana',
                    'imagen' => 'assets/compañia/11Oncependientebn.webp',
                    'description' => 'Actor y músico',
                ])

                @include('shapes.egg', [
                    'name' => 'Diana Perelli',
                    'imagen' => 'assets/compañia/1CABA373-0677-4532-BD6D-BDC3050B798D.webp',
                    'description' => 'Actriz',
                ])

                @include('shapes.egg', [
                    'name' => 'David Almaga',
                    'imagen' => 'assets/compañia/Foto_ByN_DavidA.webp',
                    'description' => ' Actor, asistente general y traspunte',
                ])

                @include('shapes.egg', [
                    'name' => 'Andrés Rivera González',
                    'imagen' => 'assets/compañia/Andres Rivera Gonzalez.webp',
                    'description' => 'Fotografía y video',
                ])

                @include('shapes.egg', [
                    'name' => 'Dan Robledo',
                    'imagen' => 'assets/compañia/20220912_130608.webp',
                    'description' => 'Ingeniero de Audio',
                ])

                @include('shapes.egg', [
                    'name' => 'Arturo Pérez Zúñiga',
                    'imagen' => 'assets/compañia/IMG_4480.webp',
                    'description' => 'Coordinación y diseño de materiales gráficos',
                ])

            </div>

            <br><br><br>

            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-12" data-aos="fade-up">
                    <h2 class="section-heading amarillo-gallinero">CIENTÍFICOS</h2>
                </div>
            </div>

            <div class="row">
                <div class="text-center col-md-6 align-items-center">
                    <img loading="lazy" src="{{ asset('assets/compañia/IMG-20190315-WA0003.webp') }}" alt="Egg Image"
                        class="img-fluid egg-image justify-content-center ">
                    <h2 style="color: gray; margin-top: 10%">Doctora Susana Alaniz</h2>
                    <p style="color: gray">Investigadora Titular “C” Coordinadora de la serie de divulgación “Experimentos
                        simples para entender una Tierra Complicada” del Instituto de Geociencias de la UNAM.</p>
                </div>

                <div class="text-center col-md-6 align-items-center">
                    <img loading="lazy" src="{{ asset('assets/compañia/Johan.webp') }}" alt="Egg Image"
                        class="img-fluid egg-image justify-content-center ">
                    <h2 style="color: gray; margin-top: 10%">Doctor Johan Van Horebeek</h2>
                    <p style="color: gray">Investigador titular “A”, SNI: Nivel II Coordinador del Área de Ciencias de la
                        Computación del Centro de Investigación en Matemáticas CIMAT A.C.
                    </p>
                </div>

                @include('shapes.egg', [
                    'name' => 'Doctor Miguel Rivas Soto',
                    'imagen' => 'assets/compañia/IMG_20200727_151721.webp',
                    'description' =>
                        'Director de la Campaña de Hábitats en Oceana México, previamente Coordinador de la Campaña de Océanos Greenpeace México A.C..',
                ])

                @include('shapes.egg', [
                    'name' => 'Ricardo Calderon Flores',
                    'imagen' => 'assets/compañia/WhatsApp Image 2022-09-11 at 9.11.05 PM.webp',
                    'description' =>
                        'Biól. Ricardo Calderón Flores, Unidad técnica operativa de Áreas Naturales Protegidas y Áreas de Valor Ambiental. Restauración Ecológica.',
                ])

                @include('shapes.egg', [
                    'name' => 'MSc. Ivete Sánchez Bravo',
                    'imagen' => 'assets/compañia/WhatsApp Image 2024-01-23 at 11.06.37 AM.webp',
                    'description' =>
                        'Personal académico, desarrollo de proyectos de innovación basados en matemáticas. Centro de Investigación en Matemáticas.',
                ])

            </div>

            {{--  <div class="row">
                <div class="col-md-4" data-aos="fade-up" data-aos-delay="">
                    <div class="text-center feature-1">
                        <div class="wrap-icon icon-1">
                            <i class="bi bi-people"></i>
                        </div>
                        <h3 class="mb-3">Explore Your Team</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="text-center feature-1">
                        <div class="wrap-icon icon-1">
                            <i class="bi bi-brightness-high"></i>
                        </div>
                        <h3 class="mb-3">Digital Whiteboard</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="text-center feature-1">
                        <div class="wrap-icon icon-1">
                            <i class="bi bi-bar-chart"></i>
                        </div>
                        <h3 class="mb-3">Design To Development</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                    </div>
                </div>
            </div>  --}}

        </div>
    </section>
    <!-- ======= CTA Section ======= -->

    <!-- Jumbotron -->
    <div class="p-4 mb-5 text-center text-white rounded bg-image shadow-1-strong"
        style="background-image: url('{{ asset('assets/prensa/Ilustr3s.Carlos Abrajam.HD.8.webp') }}'); height: 800px; margin-top: -100px;">
        <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
            <div class="d-flex justify-content-center align-items-center h-100">
                <div class="container">
                    <h3 class="mb-3 section-heading" style="color: white !important; background-color: white !important;">
                        MISIÓN
                    </h3>
                    <br><br>
                    <br>
                    <br><br>
                    <br>
                    <br><br>
                    <br>
                    <div class="container" style="max-width: 800px;">
                        <h3 class="text-white" style="word-spacing: -2px;"> Investigar, crear y enseñar el humor a través de
                            espectáculos originales
                            para
                            todas
                            las
                            edades y espacios
                            de
                            aprendizaje que promuevan momentos de catarsis, asombro, diversión y empatía que detonen la
                            reflexión y
                            el
                            pensamiento crítico.
                            A su vez, forjar alianzas con profesionistas, instituciones y organizaciones
                            sociales
                            para la divulgación de
                            sus
                            conocimientos a través del clown y la comedia física.
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jumbotron -->

    <!-- Jumbotron -->
    <div class="p-4 mb-5 text-center text-white rounded bg-image shadow-1-strong"
        style="background-image: url('{{ asset('assets/compañia/Globos-Claudia Ivonne.webp') }}'); height: 920px; margin-top: -100px;">
        <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
            <div class="d-flex justify-content-center align-items-center h-100">
                <div class="container" style="max-width: 850px;">
                    <h3 class="mb-3 section-heading"
                        style="color: white !important; background-color: white !important; word-spacing: -2px;">
                        VISIÓN
                    </h3>
                    <br><br>
                    <br><br><br>
                    <br>
                    <h3 class="text-white"> Ser un referente a nivel nacional e internacional en la creación y
                        presentación de
                        espectáculos especializados en clown y comedia física enriquecidos por otras disciplinas, siendo
                        así una
                        plataforma artística sólida para sus miembros que les permite estar en constante investigación y
                        enseñanza del humor.
                        </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- Jumbotron -->

    <!-- End CTA Section -->

    {{--  <section class="section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4 me-auto">
                        <h2 class="mb-4">Seamlessly Communicate</h2>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur at
                            reprehenderit optio,
                            laudantium eius quod, eum maxime molestiae porro omnis. Dolores aspernatur delectus impedit
                            incidunt
                            dolore mollitia esse natus beatae.</p>
                        <p><a href="#" class="btn btn-primary">Download Now</a></p>
                    </div>
                    <div class="col-md-6" data-aos="fade-left">
                        <img loading="lazy" src="assets/img/undraw_svg_2.svg" alt="Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="order-2 col-md-4 ms-auto">
                        <h2 class="mb-4">Gather Feedback</h2>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur at
                            reprehenderit optio,
                            laudantium eius quod, eum maxime molestiae porro omnis. Dolores aspernatur delectus impedit
                            incidunt
                            dolore mollitia esse natus beatae.</p>
                        <p><a href="#" class="btn btn-primary">Download Now</a></p>
                    </div>
                    <div class="col-md-6" data-aos="fade-right">
                        <img loading="lazy" src="assets/img/undraw_svg_3.svg" alt="Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </section> --}}

    <!-- ======= Testimonials Section ======= -->
    {{--  <section class="section border-top border-bottom">
            <div class="container">
                <div class="mb-5 text-center row justify-content-center">
                    <div class="col-md-4">
                        <h2 class="section-heading">Agradecemos el material fotografico otorgado por:</h2>
                    </div>
                </div>
                <div class="text-center row justify-content-center">
                    <div class="col-md-7">

                        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <div class="text-center review">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>Excellent App!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img loading="lazy" src="assets/img/person_1.jpg" alt="Image"
                                                class="mb-3 img-fluid rounded-circle">
                                            <span class="d-block">
                                                <span class="text-black">Jean Doe</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                                <div class="swiper-slide">
                                    <div class="text-center review">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>This App is easy to use!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img loading="lazy" src="assets/img/person_2.jpg" alt="Image"
                                                class="mb-3 img-fluid rounded-circle">
                                            <span class="d-block">
                                                <span class="text-black">Johan Smith</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                                <div class="swiper-slide">
                                    <div class="text-center review">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>Awesome functionality!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img loading="lazy" src="assets/img/person_3.jpg" alt="Image"
                                                class="mb-3 img-fluid rounded-circle">
                                            <span class="d-block">
                                                <span class="text-black">Jean Thunberg</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  --}}
    <!-- End Testimonials Section -->
@endsection
@section('scripts')
    @parent

    </script>
@endsection
