@extends('layouts.app')
@section('hero')
    <style>
        .hero-section {
            /* background: url('assets/contacto/8U3A5543.jpg'); */
            background: linear-gradient(to right, rgba(28, 71, 78, 0.7) 0%, rgba(28, 71, 78, 0.7) 100%), url('assets/contacto/8U3A5543.jpg');
            position: relative;
            height: 80vh;
            width: 100vw;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
        }

        .hero-section .wave {
            width: 100%;
            overflow: hidden;
            position: absolute;
            z-index: 1;
            bottom: -50px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .wave {
                bottom: -180px;
            }
        }

        .hero-section .wave svg {
            width: 100%;
        }

        .hero-section,
        .hero-section>.container>.row {
            height: 100vh;
            min-height: 880px;
        }

        .hero-section.inner-page {
            height: 60vh;
            min-height: 0;
        }

        .hero-section.inner-page .hero-text {
            transform: translateY(-150px);
            margin-top: -120px;
        }

        @media screen and (max-width: 992px) {
            .hero-section.inner-page .hero-text {
                margin-top: -80px;
            }
        }

        .hero-section h1 {
            font-size: 3.5rem;
            color: #fff;
            font-weight: 700;
            margin-bottom: 30px;
        }

        @media screen and (max-width: 992px) {
            .hero-section h1 {
                font-size: 2.5rem;
                text-align: center;
                margin-top: 40px;
            }
        }

        @media screen and (max-width: 992px) {
            .hero-section .hero-text-image {
                margin-top: 4rem;
            }
        }

        .hero-section p {
            font-size: 18px;
            color: #fff;
        }

        .hero-section .iphone-wrap {
            position: relative;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap {
                text-align: center;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            position: absolute;
            top: -50%;
            overflow: hidden;
            left: 0;
            /* box-shadow: 0 15px 50px 0 rgba(0, 0, 0, 0.3); */
            border-radius: 30px;
        }

        @media screen and (max-width: 992px) {

            .hero-section .iphone-wrap .phone-2,
            .hero-section .iphone-wrap .phone-1 {
                position: relative;
                top: 0;
                max-width: 100%;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-1 {
                margin-left: -150px;
            }
        }

        .hero-section .iphone-wrap .phone-2 {
            margin-top: 50px;
            margin-left: 100px;
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-2 {
                width: 250px;
                position: absolute;
                margin-top: 0px;
                margin-left: 100px;
            }
        }

        .egg-image {
            width: 275px;
            height: 335px;
            border-radius: 50% 50% 50% 50% / 60% 60% 40% 40%;
        }

        .img-thumbnail {
            transition: transform 0.2s ease-in-out;
        }

        .img-thumbnail:hover {
            transform: scale(1.1);
        }

        .zoom-container {
            overflow: hidden;
        }
    </style>
@endsection
@section('cabecera')
    @include('layouts.cabecera', [
        'title' => '',
        'subtitle' => '',
    ])
@endsection
@section('content')
    <!-- ======= Home Section ======= -->
    <section class="section" style="margin-top: -50px;">
        <div class="container">

            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-12" data-aos="fade-up">
                    <h2 class="section-heading verde-gallinero">CONTACTO</h2>
                    <h5 style="color: gray;" class="text-primary"><span style="font-weight: bold;">Si deseas información sobre
                            funciones, contratación de nuestros espectáculos, talleres para tu compañía, colectivo o
                            escuela… o simplemente saludar, compártenos tu información y en menos de lo que canta un gallo
                            te escribiremos:
                    </h5>
                </div>
            </div>

            @livewire('contacto-component')
        </div>
    </section>
@endsection
@section('scripts')
    @parent

    </script>
@endsection
