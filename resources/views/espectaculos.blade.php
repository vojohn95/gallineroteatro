@extends('layouts.app')
@section('hero')
    <style>
        .hero-section {
            /* background: linear-gradient(to right, rgba(3, 75, 45, 0.8) 0%, rgba(5, 100, 95, 0.8) 100%), url('assets/espectaculos/12.Ultramareadas_Andrés Rivera.jpg'); */
            background: linear-gradient(to right, rgba(28, 71, 78, 0.7) 0%, rgba(28, 71, 78, 0.7) 100%), url('assets/espectaculos/12.Ultramareadas_Andrés Rivera.jpg');
            position: relative;
            height: 80vh;
            width: 100vw;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
        }

        .hero-section .wave {
            width: 100%;
            overflow: hidden;
            position: absolute;
            z-index: 1;
            bottom: -50px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .wave {
                bottom: -180px;
            }
        }

        .hero-section .wave svg {
            width: 100%;
        }

        .hero-section,
        .hero-section>.container>.row {
            height: 100vh;
            min-height: 880px;
        }

        .hero-section.inner-page {
            height: 60vh;
            min-height: 0;
        }

        .hero-section.inner-page .hero-text {
            transform: translateY(-150px);
            margin-top: -120px;
        }

        @media screen and (max-width: 992px) {
            .hero-section.inner-page .hero-text {
                margin-top: -80px;
            }
        }

        .hero-section h1 {
            font-size: 3.5rem;
            color: #fff;
            font-weight: 700;
            margin-bottom: 30px;
        }

        @media screen and (max-width: 992px) {
            .hero-section h1 {
                font-size: 2.5rem;
                text-align: center;
                margin-top: 40px;
            }
        }

        @media screen and (max-width: 992px) {
            .hero-section .hero-text-image {
                margin-top: 4rem;
            }
        }

        .hero-section p {
            font-size: 18px;
            color: #fff;
        }

        .hero-section .iphone-wrap {
            position: relative;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap {
                text-align: center;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            position: absolute;
            top: -50%;
            overflow: hidden;
            left: 0;
            /* box-shadow: 0 15px 50px 0 rgba(0, 0, 0, 0.3); */
            border-radius: 30px;
        }

        @media screen and (max-width: 992px) {

            .hero-section .iphone-wrap .phone-2,
            .hero-section .iphone-wrap .phone-1 {
                position: relative;
                top: 0;
                max-width: 100%;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-1 {
                margin-left: -150px;
            }
        }

        .hero-section .iphone-wrap .phone-2 {
            margin-top: 50px;
            margin-left: 100px;
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-2 {
                width: 250px;
                position: absolute;
                margin-top: 0px;
                margin-left: 100px;
            }
        }

        .egg-image {
            width: 275px;
            height: 335px;
            border-radius: 50% 50% 50% 50% / 60% 60% 40% 40%;
        }

        .img-thumbnail {
            transition: transform 0.2s ease-in-out;
        }

        .img-thumbnail:hover {
            transform: scale(1.1);
        }

        .zoom-container {
            overflow: hidden;
        }
    </style>
@endsection
@section('cabecera')
    @include('layouts.cabecera', [
        'title' => '',
        'subtitle' => '',
    ])
@endsection
@section('content')
    <!-- ======= Home Section ======= -->
    <section class="section" style="margin-top: -50px;">
        <div class="container">

            <div class="mb-5 text-center row justify-content-start">
                <div class="col-md-12" data-aos="fade-up">
                    <h3 class="py-4 section-heading rojo-gallinero">¿QUÉ OFRECEMOS?</h2>
                        <h5 style="color: gray;" class="text-primary "><span
                                style="font-weight: bold; text-align: justify;">Innovación:</span> somos una
                            compañía liderada
                            por mujeres,
                            que hace espectáculos
                            originales y<br>
                            que trabaja específicamente el vínculo entre el clown, la comedia física y la ciencia.
                        </h5>
                        <br>
                        <h5 style="color: gray;" class="text-primary">
                            <span style="font-weight: bold;">Hecho a la medida:</span> <span style="text-align: justify">
                                trabajamos para la creación y difusión de
                                los valores y conocimientos de<br> nuestros
                                aliados. Facilidad de adaptación de la técnica escénica de la compañía hacia los <br>
                                conocimientos que
                                se desean difundir. </span>
                        </h5><br>
                        <h5 style="color: gray;" class="text-primary">
                            <span style="font-weight: bold;">Calidad:</span> <span style="text-align: justify">el humor
                                físico que ofrecemos está basado en más de
                                10
                                años de experiencia en la<br>técnica de clown como artistas profesionales. Nuestros
                                espectáculos
                                son de largo aliento. <br>Nuestros contenidos tienen un sustento de nuestros aliados
                                (información
                                verídica y ética).</span>
                        </h5><br>
                        <h5 style="color: gray;" class="text-primary">
                            <span style="font-weight: bold;">Desempeño:</span> <span style="text-align: justify">diseñamos
                                espectáculos de fácil movilidad, que
                                permitan
                                la optimización de <br>tiempo y recursos para presentarnos en diferentes espacios.
                            </span>
                        </h5><br>
                </div>
            </div>

            {{--  <div class="row">
                <div class="col-md-4" data-aos="fade-up" data-aos-delay="">
                    <div class="text-center feature-1">
                        <div class="wrap-icon icon-1">
                            <i class="bi bi-people"></i>
                        </div>
                        <h3 class="mb-3">Explore Your Team</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="text-center feature-1">
                        <div class="wrap-icon icon-1">
                            <i class="bi bi-brightness-high"></i>
                        </div>
                        <h3 class="mb-3">Digital Whiteboard</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                    </div>
                </div>
                <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="text-center feature-1">
                        <div class="wrap-icon icon-1">
                            <i class="bi bi-bar-chart"></i>
                        </div>
                        <h3 class="mb-3">Design To Development</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                    </div>
                </div>
            </div>  --}}

        </div>
    </section>
    <div class="mb-5 text-center row justify-content-center">
        <div class="col-md-12" data-aos="fade-up">
            <h2 class="section-heading amarillo-gallinero">ESPECTÁCULOS</h2>
        </div>
    </div>

    <div class="card-group">
        <div class="text-center card">
            <img loading="lazy" src="{{ asset('assets/espectaculos/DSC01376.webp') }}" class="card-img-top img-fluid"
                alt="Hollywood Sign on The Hill" />
            <div class="card-body">
                <h2 class="card-title fw-bold">Desesperimentos</h2>
                <p class="card-text">
                    Una conferencia fuera, muy fuera de lo convencional.
                </p>
                <!-- Button trigger modal -->
                <button type="button" class="btn fondo-verde-gallinero btn-rounded" data-mdb-ripple-init
                    data-mdb-modal-init data-mdb-target="#desesperimentos">
                    <span class="icon" id="icon1" style="color: white;">
                        <i class="bi bi-egg-fill" id="eggIcon1"></i> VER MÁS
                    </span>
                </button>
            </div>
        </div>
        <div class="text-center card">
            <img loading="lazy" src="{{ asset('assets/espectaculos/Ilustr3s.Carlos Abrajam.HD.10.webp') }}" class="card-img-top img-fluid"
                alt="Palm Springs Road" />
            <div class="card-body">
                <h2 class="card-title fw-bold">Tr3s Ilustr3s</h2>
                <p class="card-text">
                    Las matemáticas como nunca las habías visto.
                </p>
                <!-- Button trigger modal -->
                <button type="button" class="btn fondo-verde-gallinero btn-rounded" data-mdb-ripple-init
                    data-mdb-modal-init data-mdb-target="#ilustres">
                    <span class="icon" id="icon1" style="color: white;">
                        <i class="bi bi-egg-fill" id="eggIcon1"></i> VER MÁS
                    </span>
                </button>
            </div>
        </div>
    </div>

    <div class="card-group">
        <div class="text-center card">
            <img loading="lazy" src="{{ asset('assets/espectaculos/DSC03748.webp') }}" class="card-img-top img-fluid"
                alt="Hollywood Sign on The Hill" />
            <div class="card-body">
                <h2 class="card-title fw-bold">Ultramareadas</h2>
                <p class="card-text">
                    Sol, arena y mar… ¿y basura?
                </p>
                <!-- Button trigger modal -->
                <button type="button" class="btn fondo-verde-gallinero btn-rounded" data-mdb-ripple-init
                    data-mdb-modal-init data-mdb-target="#ultramareadas">
                    <span class="icon" id="icon1" style="color: white;">
                        <i class="bi bi-egg-fill" id="eggIcon1"></i> VER MÁS
                    </span>
                </button>
            </div>
        </div>
        <div class="text-center card">
            <img loading="lazy" src="{{ asset('assets/espectaculos/5.Buenos Días Somos Sus Guías_El Gallinero_Foto Andrés Rivera.webp') }}"
                class="card-img-top img-fluid" alt="Palm Springs Road" />
            <div class="card-body">
                <h2 class="card-title fw-bold">Buenos Días, Somos Sus Guías</h2>
                <p class="card-text">
                    El equilibrio ecológico en las mejores manos… ¿o no?
                </p>
                <!-- Button trigger modal -->
                <button type="button" class="btn fondo-verde-gallinero btn-rounded" data-mdb-ripple-init
                    data-mdb-modal-init data-mdb-target="#buenosdias">
                    <span class="icon" id="icon1" style="color: white;">
                        <i class="bi bi-egg-fill" id="eggIcon1"></i> VER MÁS
                    </span>
                </button>
            </div>
        </div>
    </div>

    <br><br>

    <div class="pt-4 mb-5 text-center row justify-content-center">
        <div class="col-md-12" data-aos="fade-up">
            <h2 class="section-heading rojo-gallinero">DIVULGACIÓN RECREATIVA</h2>
        </div>
    </div>

    <div class="container">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col ">
                <div class="card h-100">
                    <img loading="lazy" src="{{ asset('assets/espectaculos/Portada Canción Pi.webp') }}" class="card-img-top"
                        alt="Skyscrapers" style="height: 280px;" />
                    <div class="card-body">
                        <h5 class="text-center card-title">Canción de Pi (2021)</h5>
                        <p class="card-text">
                            Esta es una bella canción de Pi (3.14) que El Gallinero Teatro Clown y El Centro de
                            Investigación en
                            Matemáticas (CIMAT) te comparten aquí.
                        </p>
                    </div>
                    <div class="text-center card-footer">
                        <a href="https://youtu.be/e4QfoLQubc4" target="_blank"
                            class="btn fondo-verde-gallinero btn-rounded">
                            <span class="icon" id="icon1" style="color: white;">
                                <i class="bi bi-egg-fill" id="eggIcon1"></i> VER
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card h-100">
                    <img loading="lazy" src="{{ asset('assets/espectaculos/Portada La luz y los colores.webp') }}" class="card-img-top"
                        alt="Los Angeles Skyscrapers" style="height: 280px;" />
                    <div class="card-body">
                        <h5 class="text-center card-title">La luz y los colores (2019)</h5>
                        <p class="card-text">
                            Video que combina el Clown con la Física para conocer la descomposición, reflexión y refracción
                            de la luz de una manera divertida. Una colaboración de El Gallinero Teatro Clown y el Centro de
                            Geociencias
                            UNAM.
                        </p>
                    </div>
                    <div class="text-center card-footer">
                        <a href="https://youtu.be/QSgDMFedpbs" target="_blank"
                            class="btn fondo-verde-gallinero btn-rounded">
                            <span class="icon" id="icon1" style="color: white;">
                                <i class="bi bi-egg-fill" id="eggIcon1"></i> VER
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card h-100">
                    <img loading="lazy" src="{{ asset('assets/espectaculos/Portada La Deriva.webp') }}" style="height: 280px;"
                        class="card-img-top" alt="Palm Springs Road" />
                    <div class="card-body">
                        <h5 class="text-center card-title">A la deriva (2016)</h5>
                        <p class="card-text">
                            Video que combina el Clown con la Física para conocer acerca de la deriva continental de una
                            manera sencilla y lúdica. Una colaboración de El Gallinero Teatro Clown y el Centro de
                            Geociencias de la UNAM.
                        </p>
                    </div>
                    <div class="text-center card-footer">
                        <a href="https://youtu.be/EClpvClNAp8" target="_blank"
                            class="btn fondo-verde-gallinero btn-rounded">
                            <span class="icon" id="icon1" style="color: white;">
                                <i class="bi bi-egg-fill" id="eggIcon1"></i> VER
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <br><br>

        <div class="pt-8 mt-8 row row-cols-1 row-cols-md-3 g-4">
            <div class="col">
                <div class="card h-100">
                    <img loading="lazy" src="{{ asset('assets/espectaculos/Portada vamos a hacer algo.webp') }}" style="height: 280px;"
                        class="card-img-top" alt="Palm Springs Road" />
                    <div class="card-body">
                        <h5 class="text-center card-title">¡Vamos a hacer algo! (2021)</h5>
                        <p class="card-text">
                            En esta infografía encontrarás 6 valiosas acciones responsables ante el consumo cotidiano y
                            desmedido de plásticos de un solo uso.
                        </p>
                    </div>
                    <div class="text-center card-footer">
                        <a href="https://drive.google.com/file/d/14G2LOurLbhx1JLKCMWzUXxMPsBkVnUhx/view" target="_blank"
                            class="btn fondo-verde-gallinero btn-rounded">
                            <span class="icon" id="icon1" style="color: white;">
                                <i class="bi bi-egg-fill" id="eggIcon1"></i> DESCARGAR
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card h-100">
                    <img loading="lazy" src="{{ asset('assets/espectaculos/Portada para  Momentos Ilustres.webp') }}"
                        style="height: 280px;" class="card-img-top" alt="Palm Springs Road" />
                    <div class="card-body">
                        <h5 class="text-center card-title">Momentos Ilustres (2020)
                        </h5>
                        <p class="card-text">
                            Este librito trata de responder ¿qué se estudia en matemáticas? A través de ensayos cortos que
                            parten de su entorno cotidiano: la ciudad de Guanajuato.
                        </p>
                    </div>
                    <div class="text-center card-footer">
                        <a href="https://www.cimat.mx/producto/momentos-ilustres/" target="_blank"
                            class="btn fondo-verde-gallinero btn-rounded">
                            <span class="icon" id="icon1" style="color: white;">
                                <i class="bi bi-egg-fill" id="eggIcon1"></i> IR
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card h-100">
                    <img loading="lazy" src="{{ asset('assets/espectaculos/Portada Experimentos Simples.webp') }}" style="height: 280px;"
                        class="card-img-top" alt="Palm Springs Road" />
                    <div class="card-body">
                        <h5 class="text-center card-title">Experimentos simples para entender una Tierra complicada (2002)

                        </h5>
                        <p class="card-text">
                            Serie de libros en los que encontrarás diferentes y sencillos experimentos científicos que
                            puedes realizar en casa, en la escuela y hasta en el parque.
                        </p>
                    </div>
                    <div class="text-center card-footer">
                        <a href="https://tellus.geociencias.unam.mx/index.php/serie/" target="_blank"
                            class="btn fondo-verde-gallinero btn-rounded">
                            <span class="icon" id="icon1" style="color: white;">
                                <i class="bi bi-egg-fill" id="eggIcon1"></i> IR
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>

    <!-- Jumbotron -->
    <div class="p-4 mb-5 text-center text-white rounded bg-image shadow-1-strong img-fluid"
        style="background-image: url('{{ asset('assets/espectaculos/grupo.webp') }}'); background-size: 100% 100%; background-position: center; height: 1000px;">
        <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
            <div class="d-flex justify-content-center align-items-center h-75">
                <div class="container">
                    <h2 class="mb-0 text-white">TALLERES</h2>
                    <br><br><br><br>
                    <h3 class="text-white"> ¿Quieres escuchar sobre nuestra oferta de talleres o te interesa un perfil en
                        particular?
                        ¡Contáctanos! En El Gallinero Teatro Clown tenemos experiencia facilitando talleres para el estudio
                        del clown,
                        asesoría a otras compañías escénicas, actividades para empresas o para armar tu combo con un
                        espectáculo y reforzar de forma divertida sus contenidos. Compártenos tu interés y detalles del
                        grupo a trabajar.
                    </h3>
                    <br><br><br><br><br>
                    <a href="{{ url('contacto') }}" class="btn fondo-verde-gallinero btn-rounded"><span class="icon"
                            id="icon1" style="color: white;">
                            <i class="bi bi-egg-fill" id="eggIcon1"></i>
                            ¡CONTÁCTANOS!</span> </a>
                </div>
            </div>
        </div>
    </div>



    <!-- Jumbotron -->

    @include('modals.espectaculos.desesperimentos')
    @include('modals.espectaculos.ilustres')
    @include('modals.espectaculos.ultramareadas')
    @include('modals.espectaculos.buenosdias')
@endsection
@section('scripts')
    @parent

    </script>
@endsection
