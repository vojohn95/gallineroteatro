<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>El Gallinero Teatro Clown</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('assets/Profile.png') }}" rel="icon">
    <link href="{{ asset('assets/Profile.png') }}" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">

    <!-- Google Fonts -->
    <!-- <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet"> -->

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/7.1.0/mdb.min.css" rel="stylesheet" />

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

    @livewireStyles

    @yield('hero')

    <style>
        body {
            font-family: 'Lato', sans-serif;
        }

        h1,
        h2,
        p {
            font-family: 'Lato', sans-serif;
        }
    </style>
</head>

<body>

    @livewire('nav-component')

    <!-- ======= Hero Section ======= -->
    {{--  <section class="hero-section" id="hero">  --}}
    <section id="hero">
        @yield('cabecera')
    </section>
    <!-- End Hero -->

    <main id="main">
        @yield('content')

    </main><!-- End #main -->

    @include('layouts.navs.footer')

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <!-- MDB -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/7.1.0/mdb.umd.min.js"></script>

    @livewireScripts

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <x-livewire-alert::scripts />

    <script>
        // Obtener todos los iconos y contenedores
        const icons = document.querySelectorAll('.icon');

        // Iterar sobre cada icono y contenedor
        icons.forEach((iconContainer, index) => {
            const icon = iconContainer.querySelector('i');

            // Escuchar el evento mouseenter en el contenedor
            iconContainer.addEventListener('mouseenter', function() {
                // Cambiar la clase del icono al pasar el mouse sobre él
                icon.className = 'bi bi-egg-fried';
            });

            // Escuchar el evento mouseleave en el contenedor
            iconContainer.addEventListener('mouseleave', function() {
                // Revertir la clase del icono al quitar el mouse
                icon.className = 'bi bi-egg-fill';
            });
        });
    </script>

</body>

</html>
