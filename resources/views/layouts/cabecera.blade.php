<section class="hero-section" id="hero">
    <div class="wave">
        <svg width="60%" height="60%" id="svg" viewBox="0 0 1440 320" xmlns="http://www.w3.org/2000/svg"
            class="wave-svg">
            <g transform="scale(-1, 1) translate(-1440, 0)">
                <path
                    d="M 0,500 L 0,187 C 131.7333333333333,153.53333333333333 263.4666666666666,120.06666666666666 449,144 C 634.5333333333334,167.93333333333334 873.8666666666668,249.26666666666665 1048,266 C 1222.1333333333332,282.73333333333335 1331.0666666666666,234.86666666666667 1440,187 L 1440,500 L 0,500 Z"
                    stroke="none" stroke-width="0" fill="white" fill-opacity="1" </path>
            </g>
        </svg>
    </div>

    <div class="container">
        <div class="row align-items-center min-vh-100">
            <div class="text-center col-12 hero-text-image">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 data-aos="fade-right" class="justify-content-center"><span><strong>{{ $title }}</strong></span></h1>
                        <h3 class="mb-5 text-white text-lg-end" data-aos="fade-right" data-aos-delay="100">
                            {{ $subtitle }}
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
