<style>
    .footer-container {
        position: relative;
    }

    .profile-img {
        position: absolute;
        top: 72%;
        left: 25%;
        transform: translate(-50%, -50%) rotate(340deg);
        z-index: 2;
    }

    /* Media Query para pantallas de celular */
    @media only screen and (max-width: 767px) {
        .profile-img {
            top: 30%;
            left: 25%;
            transform: translate(-50%, -50%) rotate(345deg);
        }
    }

    /* Media Query para escritorio pequeño */
    @media only screen and (min-width: 768px) and (max-width: 991px) {
        .profile-img {
            top: 60%;
            left: 25%;
            transform: translate(-50%, -50%) rotate(340deg);
        }
    }

    /* Media Query para escritorio grande */
    @media only screen and (min-width: 992px) {
        .profile-img {
            top: 72%;
            left: 25%;
            transform: translate(-50%, -50%) rotate(340deg);
        }
    }
</style>
<hr>

<section class="section">
    <div class="container">
        {{--  <div class="mb-5 text-center row justify-content-center" data-aos="fade">
                    <div class="mb-5 col-md-6">
                        <img src="assets/img/undraw_svg_1.svg" alt="Image" class="img-fluid">
                    </div>
                </div>  --}}

        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    {{--  <span class="number">01</span>  --}}
                    <h2 style="font: gray;">¡Síguenos en nuestras redes sociales!</h3>
                        <a href="https://www.instagram.com/elgallinero_teatro/" target="_blank" type="button"
                            class="btn btn-circle btn-xl" style="background-color: gray; color:white;">
                            <i class="bi bi-instagram"></i>
                        </a>
                        &nbsp;
                        <a href="https://www.facebook.com/ElGallineroTeatro" target="_blank" type="button"
                            class="btn btn-circle btn-xl" style="background-color: gray; color:white;">
                            <i class="bi bi-facebook"></i>
                        </a>
                        &nbsp;
                        <a href="https://www.youtube.com/channel/UCm1gxrCzCnzBl3KwCKGK8oQ/featured" target="_blank"
                            type="button" class="btn btn-circle btn-xl" style="background-color: gray; color:white;"><i
                                class="bi bi-youtube"></i>
                        </a>
                        &nbsp;
                        <a href="https://www.tiktok.com/@elgallineroteatro" target="_blank" type="button"
                            class="btn btn-circle btn-xl" style="background-color: gray; color:white;"><i
                                class="bi bi-tiktok"></i>
                        </a>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    {{--  <div class="col-md-4">
                        <div class="step">
                            <span class="number">02</span>
                            <h3>Create Profile</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="step">
                            <span class="number">03</span>
                            <h3>Enjoy the app</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                        </div>
                    </div>  --}}
    </div>
    </div>

</section>


<div class="footer-container ">
    <img loading="lazy" src="{{ asset('assets/Eggy.png') }}" width="200px;" class="img-fluid profile-img">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" class="svg-background ">
        <path fill="#01adb6" fill-opacity="1"
            d="M0,64L48,106.7C96,149,192,235,288,261.3C384,288,480,256,576,218.7C672,181,768,139,864,106.7C960,75,1056,53,1152,58.7C1248,64,1344,96,1392,112L1440,128L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z">
        </path>
    </svg>
</div>

<footer class="footer" role="contentinfo" style="background-color: #01adb6;">
    <div class="container">
        <div class="row">
            <div class="mb-4 text-white col-md-4 mb-md-0">
                <h3 class="text-white">Contacto</h3>
                <p>gallinero.contacto@gmail.com</p>
                <p class="social">
                    <a href="https://www.youtube.com/channel/UCm1gxrCzCnzBl3KwCKGK8oQ/featured" target="_blank"><span
                            class="bi bi-youtube"></span></a>
                    <a href="https://www.facebook.com/ElGallineroTeatro" target="_blank"><span
                            class="bi bi-facebook"></span></a>
                    <a href="https://www.instagram.com/elgallinero_teatro/" target="_blank"><span
                            class="bi bi-instagram"></span></a>
                    <a href="https://www.tiktok.com/@elgallineroteatro" target="_blank"><span
                            class="bi bi-tiktok"></span></a>
                </p>
            </div>
            <div class="col-md-7 ms-auto">
                <div class="pt-0 row site-section">
                    <div class="mb-4 col-md-4 mb-md-0">
                        {{--  <h3 class="text-white">Navigation</h3>
                        <ul class="list-unstyled">
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>  --}}
                    </div>
                    <div class="mb-4 col-md-4 mb-md-0">
                        {{--  <h3 class="text-white">Services</h3>
                        <ul class="list-unstyled">
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Collaboration</a></li>
                            <li><a href="#">Todos</a></li>
                            <li><a href="#">Events</a></li>
                        </ul>  --}}
                    </div>
                    <div class="mb-4 col-md-4 mb-md-0">
                        <h3 class="text-white">Enlaces</h3>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('compañia') }}">La compañía</a></li>
                            <li><a href="{{ url('espectaculos') }}">Espectáculos</a></li>
                            <li><a href="{{ url('prensa') }}">Prensa</a></li>
                            <li><a href="{{ url('cartelera') }}">Cartelera</a></li>
                            <li><a href="{{ url('contacto') }}">Contacto</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center row justify-content-center">
            <div class="col-md-7">
                <div class="credits">
                    Developed by <a href="https://www.linkedin.com/in/johnvo95" target="_blank">John</a>
                </div>
            </div>
        </div>

    </div>
</footer>

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
        class="bi bi-arrow-up-short"></i></a>
