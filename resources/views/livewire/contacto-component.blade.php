<div>
    <form wire:submit="submit">
        <!-- 2 column grid layout with text inputs for the first and last names -->
        <div class="mb-4 row">
            <div class="col">
                <div data-mdb-input-init class="form-outline">
                    <input type="text" id="form6Example1" class="form-control" wire:model='nombre' required />
                    <label class="form-label" for="form6Example1">Nombre</label>
                </div>
            </div>
            <div class="col">
                <div data-mdb-input-init class="form-outline">
                    <input type="number" min="0" max="100" id="form6Example2" class="form-control"
                        wire:model='edad' required />
                    <label class="form-label" for="form6Example2">Edad</label>
                </div>
            </div>
        </div>

        <!-- Text input -->
        <div data-mdb-input-init class="mb-4 form-outline">
            <input type="text" id="form6Example3" class="form-control" wire:model='institucion' required />
            <label class="form-label" for="form6Example3">Compañia / Institución a la que perteneces</label>
        </div>

        <!-- Text input -->
        <div data-mdb-input-init class="mb-4 form-outline">
            <input type="email" id="form6Example4" class="form-control" wire:model='email' required />
            <label class="form-label" for="form6Example4">Correo electrónico</label>
        </div>

        <!-- Email input -->
        <div data-mdb-input-init class="mb-4 form-outline">
            <input type="text" id="form6Example5" class="form-control" wire:model='telefono' />
            <label class="form-label" for="form6Example5">Teléfono (opcional)</label>
        </div>

        <!-- Number input -->
        <div data-mdb-input-init class="mb-4 form-outline">
            <input type="text" id="form6Example6" class="form-control" wire:model='asunto' required />
            <label class="form-label" for="form6Example6">Desea recibir información de ...</label>
        </div>

        <!-- Message input -->
        <div data-mdb-input-init class="mb-4 form-outline">
            <textarea class="form-control" id="form6Example7" rows="4" wire:model='mensaje' required></textarea>
            <label class="form-label" for="form6Example7">Comentarios adicionales:</label>
        </div>


        <!-- Submit button -->
        <button data-mdb-ripple-init type="submit" class="mb-4 btn fondo-rojo-gallinero btn-rounded btn-block"><span
                class="icon" id="icon1" style="color: white;">
                <i class="bi bi-egg-fill" id="eggIcon1"></i> ENVIAR</button>
    </form>
</div>
