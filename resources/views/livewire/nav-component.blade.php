<div>
    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-left">
        <div class="container d-flex justify-content-between align-items-left">

            <div class="logo" style="margin-top: 2px;">
                <h1><a href="{{ url('/') }}"><img loading="lazy" src="{{ asset('assets/Logo_Blanco_Transparente.png') }}"
                            width="120px;" height="100px;" class="img-fluid"></a>
                </h1>
            </div>

            <nav id="navbar" class="navbar" style="box-shadow: none; !important;">
                <ul>
                    {{--  <li><a class="active " href="index.html">Home</a></li>  --}}
                    <li><a href="{{ url('compañia') }}">Compañía</a></li>
                    <li><a href="{{ url('espectaculos') }}">Espectáculos</a></li>
                    {{--  <li><a href="">Divulgación</a></li>  --}}
                    <li><a href="{{ url('prensa') }}">Prensa</a></li>
                    <li><a href="{{ url('cartelera') }}">Cartelera</a></li>
                    {{--  <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="#">Drop Down 1</a></li>
                        <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i
                                    class="bi bi-chevron-right"></i></a>
                            <ul>
                                <li><a href="#">Deep Drop Down 1</a></li>
                                <li><a href="#">Deep Drop Down 2</a></li>
                                <li><a href="#">Deep Drop Down 3</a></li>
                                <li><a href="#">Deep Drop Down 4</a></li>
                                <li><a href="#">Deep Drop Down 5</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Drop Down 2</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                    </ul>
                </li>  --}}
                    <li><a href="{{ url('contacto') }}">Contacto</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->

</div>
