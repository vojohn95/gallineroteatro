<!-- Modal -->
<div class="modal fade" id="ilustres" tabindex="-1" aria-labelledby="ilustresLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ilustresLabel">Tr3s Ilustr3s</h5>
                <button type="button" class="btn-close" data-mdb-ripple-init data-mdb-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Carousel wrapper -->
                <div id="carouselilustres" class="carousel slide carousel-fade" data-mdb-ride="carousel"
                    data-mdb-carousel-init>
                    <!-- Indicators -->
                    <div class="carousel-indicators">
                        <button type="button" data-mdb-target="#carouselilustres" data-mdb-slide-to="0" class="active"
                            aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-mdb-target="#carouselilustres" data-mdb-slide-to="1"
                            aria-label="Slide 2"></button>
                        <button type="button" data-mdb-target="#carouselilustres" data-mdb-slide-to="2"
                            aria-label="Slide 3"></button>
                    </div>

                    <!-- Inner -->
                    <div class="carousel-inner rounded-5 shadow-4-strong">
                        <!-- Single item -->
                        <div class="carousel-item active">
                            <img loading="lazy" src="{{ asset('assets/espectaculos/illustres/Ilustr3.Pili Pala.HD.1.jpg') }}"
                                class="d-block w-100" alt="Sunset Over the City" />
                            <div class="carousel-caption d-none d-md-block">
                                {{--  <h5>First slide label</h5>
                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>  --}}
                            </div>
                        </div>

                        <!-- Single item -->
                        <div class="carousel-item">
                            <img  loading="lazy" src="{{ asset('assets/espectaculos/illustres/Ilustr3.Pili Pala.HD.3.jpg') }}"
                                class="d-block w-100" alt="Canyon at Nigh" />
                            <div class="carousel-caption d-none d-md-block">
                                {{--  <h5>Second slide label</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>  --}}
                            </div>
                        </div>

                        <!-- Single item -->
                        <div class="carousel-item">
                            <img loading="lazy" src="{{ asset('assets/espectaculos/illustres/Ilustr3s.Carlos Abrajam.HD.11.jpg') }}"
                                class="d-block w-100" alt="Cliff Above a Stormy Sea" />
                            <div class="carousel-caption d-none d-md-block">
                                {{--  <h5>Third slide label</h5>
                                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>  --}}
                            </div>
                        </div>
                    </div>
                    <!-- Inner -->

                    <!-- Controls -->
                    <button class="carousel-control-prev" type="button" data-mdb-target="#carouselilustres"
                        data-mdb-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-mdb-target="#carouselilustres"
                        data-mdb-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
                <!-- Carousel wrapper -->
            </div>
            <div class="container">
                <p class="card-text">
                    Espectáculo de teatro-clown para hablar de matemáticas con buen humor.
                    <br><br>
                    Tres ilustres personas muestran por medio de música, juegos y un flechazo al corazón, algunos
                    conceptos matemáticos que forman parte de la vida cotidiana y de la no tan cotidiana; con el
                    objetivo de ver esta ciencia exacta como una oportunidad de diversión y de placentero aprendizaje.

                    <br><br>
                    Público: de 14 años en adelante
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn fondo-verde-gallinero" data-mdb-ripple-init
                    data-mdb-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
