<!-- Modal -->
<div class="modal fade" id="ultramareadas" tabindex="-1" aria-labelledby="ultramareadasLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ultramareadasLabel">Ultramareadas</h5>
                <button type="button" class="btn-close" data-mdb-ripple-init data-mdb-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Carousel wrapper -->
                <div id="carouselultramareadas" class="carousel slide carousel-fade" data-mdb-ride="carousel"
                    data-mdb-carousel-init>
                    <!-- Indicators -->
                    <div class="carousel-indicators">
                        <button type="button" data-mdb-target="#carouselultramareadas" data-mdb-slide-to="0"
                            class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-mdb-target="#carouselultramareadas" data-mdb-slide-to="1"
                            aria-label="Slide 2"></button>
                        <button type="button" data-mdb-target="#carouselultramareadas" data-mdb-slide-to="2"
                            aria-label="Slide 3"></button>
                    </div>

                    <!-- Inner -->
                    <div class="carousel-inner rounded-5 shadow-4-strong">
                        <!-- Single item -->
                        <div class="carousel-item active">
                            <img loading="lazy" src="{{ asset('assets/espectaculos/ultramareadas/2.Ultramareadas_Andrés Rivera.jpg') }}"
                                class="d-block w-100" alt="Sunset Over the City" />
                            <div class="carousel-caption d-none d-md-block">
                                {{--  <h5>First slide label</h5>
                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>  --}}
                            </div>
                        </div>

                        <!-- Single item -->
                        <div class="carousel-item">
                            <img loading="lazy" src="{{ asset('assets/espectaculos/ultramareadas/1. ULTRAMAREADAS_Andrés Rivera.jpg') }}"
                                class="d-block w-100" alt="Canyon at Nigh" />
                            <div class="carousel-caption d-none d-md-block">
                                {{--  <h5>Second slide label</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>  --}}
                            </div>
                        </div>

                        <!-- Single item -->
                        <div class="carousel-item">
                            <img loading="lazy" src="{{ asset('assets/espectaculos/ultramareadas/4.Ultramareadas_Andrés Rivera.jpg') }}"
                                class="d-block w-100" alt="Cliff Above a Stormy Sea" />
                            <div class="carousel-caption d-none d-md-block">
                                {{--  <h5>Third slide label</h5>
                                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>  --}}
                            </div>
                        </div>
                    </div>
                    <!-- Inner -->

                    <!-- Controls -->
                    <button class="carousel-control-prev" type="button" data-mdb-target="#carouselultramareadas"
                        data-mdb-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-mdb-target="#carouselultramareadas"
                        data-mdb-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
                <!-- Carousel wrapper -->
            </div>
            <div class="container">
                <p class="card-text">
                    Espectáculo de teatro-clown para hablar de océanos libres de plásticos con buen humor.
                    <br><br>
                    En un chapuzón de diversión, tres actrices y un músico viven aventuras acuáticas a través del clown,
                    música en vivo y juegos con el público. Con títeres e instrumentos musicales creados con objetos en
                    desuso, ULTRAMAREADAS reflexiona sobre la responsabilidad que tenemos en el cuidado del medio
                    ambiente.

                    <br><br>
                    Público: de 6 años en adelante

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn fondo-verde-gallinero" data-mdb-ripple-init
                    data-mdb-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
