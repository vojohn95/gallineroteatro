@extends('layouts.app')
@section('hero')
    <style>
        .hero-section {
            background: linear-gradient(to right, rgba(28, 71, 78, 0.7) 0%, rgba(28, 71, 78, 0.7) 100%), url('assets/prensa/Ilustr3s.Carlos Abrajam.HD.2.jpg');
            position: relative;
            height: 80vh;
            width: 100vw;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
        }

        .hero-section .wave {
            width: 100%;
            overflow: hidden;
            position: absolute;
            z-index: 1;
            bottom: -50px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .wave {
                bottom: -180px;
            }
        }

        .hero-section .wave svg {
            width: 100%;
        }

        .hero-section,
        .hero-section>.container>.row {
            height: 100vh;
            min-height: 880px;
        }

        .hero-section.inner-page {
            height: 60vh;
            min-height: 0;
        }

        .hero-section.inner-page .hero-text {
            transform: translateY(-150px);
            margin-top: -120px;
        }

        @media screen and (max-width: 992px) {
            .hero-section.inner-page .hero-text {
                margin-top: -80px;
            }
        }

        .hero-section h1 {
            font-size: 3.5rem;
            color: #fff;
            font-weight: 700;
            margin-bottom: 30px;
        }

        @media screen and (max-width: 992px) {
            .hero-section h1 {
                font-size: 2.5rem;
                text-align: center;
                margin-top: 40px;
            }
        }

        @media screen and (max-width: 992px) {
            .hero-section .hero-text-image {
                margin-top: 4rem;
            }
        }

        .hero-section p {
            font-size: 18px;
            color: #fff;
        }

        .hero-section .iphone-wrap {
            position: relative;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap {
                text-align: center;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            position: absolute;
            top: -50%;
            overflow: hidden;
            left: 0;
            /* box-shadow: 0 15px 50px 0 rgba(0, 0, 0, 0.3); */
            border-radius: 30px;
        }

        @media screen and (max-width: 992px) {

            .hero-section .iphone-wrap .phone-2,
            .hero-section .iphone-wrap .phone-1 {
                position: relative;
                top: 0;
                max-width: 100%;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-1 {
                margin-left: -150px;
            }
        }

        .hero-section .iphone-wrap .phone-2 {
            margin-top: 50px;
            margin-left: 100px;
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-2 {
                width: 250px;
                position: absolute;
                margin-top: 0px;
                margin-left: 100px;
            }
        }

        .egg-image {
            width: 275px;
            height: 335px;
            border-radius: 50% 50% 50% 50% / 60% 60% 40% 40%;
        }

        .img-thumbnail {
            transition: transform 0.2s ease-in-out;
        }

        .img-thumbnail:hover {
            transform: scale(1.1);
        }

        .zoom-container {
            overflow: hidden;
        }
    </style>
@endsection
@section('cabecera')
    @include('layouts.cabecera', [
        'title' => '',
        'subtitle' => '',
    ])
@endsection
@section('content')
    <!-- ======= Home Section ======= -->
    <section class="section" style="margin-top: -50px;">
        <div class="container">

            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-12" data-aos="fade-up">
                    <h2 class="section-heading rojo-gallinero">ULTRAMAREADAS</h2>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;" style="text-align: left;">“Entrar al mundo de la
                                ciencia y el medio
                                ambiente es más
                                divertido de lo que se cree, esto lo comprueba la compañía El Gallinero Teatro, con la obra
                                Ultramareadas…”</h5>
                            <p class="text-justify card-text">Reporte Índigo, 2021 </p>
                            <a href="https://drive.google.com/file/d/1JTFP4KQLuzykecsqF3a0Kee23dyyEzrM/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded "
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;">“Ultramareadas lleva al público a las
                                profundidades del mar para alertar
                                sobre su cuidado.”</h5>
                            <p class="text-justify card-text">La Jornada, 2021</p>
                            <a href="https://drive.google.com/file/d/1EzZT2nHj6O4Dn-qIBpToUpLGnVKYnlXo/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded"
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;">“Gallinero Teatro da a la audiencia información
                                necesaria para
                                estructurar sus propias respuestas y opciones, construyendo un ejercicio de conciencia muy
                                divertido y lleno de colores brillantes y energía.”</h5>
                            <p class="text-justify card-text">Menú Teatral, 2021
                            </p>
                            <a href="https://drive.google.com/file/d/1fyn6W9jXd26ec9CRNs_XGkgpr7FPIV54/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded"
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <h2 class="section-heading rojo-gallinero">
                        TR3S ILUSTR3S
                    </h2>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;">“Con obra lúdica despiertan la curiosidad de
                                niños y jóvenes por las
                                matemáticas.” </h5>
                            <p class="text-justify card-text">La Jornada, 2018</p>
                            <a href="https://drive.google.com/file/d/1DnuraKNe8T5N9hSbc8TXeWJQ-0wonFzo/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded"
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;">“Una propuesta que contiene todos los
                                ingredientes necesarios al abordar
                                una de las ciencias exactas como son las matemáticas, de una manera sencilla, desde
                                acertijos, hasta figuras geométricas.”</h5>
                            <p class="text-justify card-text">Uniradio, 2018
                            </p>
                            <a href="https://drive.google.com/file/d/1Iec4wELq2pldjvF_Si6uZnmxwyZ-l88g/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded"
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;">“Las matemáticas invadirán la Sala Xavier
                                Villaurrutia para mostrar que
                                no son tan aburridas como la mayoría cree.”</h5>
                            <p class="text-justify card-text">TimeOut, 2018
                            </p>
                            <a href="https://drive.google.com/file/d/1hFpbV-ULfxlvMyPgEq488NujjtTmmtoZ/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded"
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <h2 class="section-heading rojo-gallinero">
                        DESESPERIMENTOS
                    </h2>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;">“De manera divertida, una científica y su
                                asistente hicieron diversos
                                experimentos en los que explicaron algunos conceptos de la física realizados por famosos
                                investigadores como lo fueron Galileo y Newton.”</h5>
                            <p class="text-justify card-text">Periódico Correo, 2014</p>
                            <a href="https://drive.google.com/file/d/1WeRSrPngECj412slQdCqlGcd2nyFjOBx/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded"
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;">“Enseñan ciencia con teatro para niños.”
                            </h5>
                            <p class="text-justify card-text">Es lo cotidiano, 2014
                            </p>
                            <a href="https://drive.google.com/file/d/1eezmGvdRj3OpssAj8Nhavc-hlteZ6DJ5/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded"
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title" style="text-align: left;">“La obra mostró de manera lúdica conceptos
                                físicos a los alumnos.” </h5>
                            <p class="text-justify card-text">Pasos del Sur, 2014

                            </p>
                            <a href="https://drive.google.com/file/d/1Vpx2gVZlB04cX4h7O71ybyyeQTOApPxY/view?usp=sharing"
                                target="_blank" type="button" class="text-left btn fondo-verde-gallinero btn-rounded"
                                data-mdb-ripple-init>
                                <span class="icon" id="icon1" style="color: white;">
                                    <i class="bi bi-egg-fill" id="eggIcon1"></i> LEER MÁS
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    @parent

    </script>
@endsection
