<style>
    .image-container {
        max-width: 100%;
        height: auto;
        display: inline-block;
        overflow: hidden;
    }
</style>

<div class="text-center col-md-4 align-items-center">
    <div class="image-container">
        <img loading="lazy" src="{{ asset($imagen) }}" alt="Egg Image" class="img-fluid egg-image justify-content-center">
    </div>
    <h2 style="color: gray; margin-top: 10%">{{ $name }}</h2>
    @if (isset($description))
        <p style="color: gray">{{ $description }}</p>
    @endif
</div>
