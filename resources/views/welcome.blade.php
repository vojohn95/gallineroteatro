@extends('layouts.app')
@section('hero')
    <style>
        /*--------------------------------------------------------------
                                            # Hero Section
                                            --------------------------------------------------------------*/
        .hero-section {
            /* background: linear-gradient(to right, rgba(49, 51, 53, 0.8) 0%, rgba(30, 32, 32, 0.8) 100%), url(../img/hero-bg.jpg);
                                            */
            /* position: relative; */
            height: 100vh;
            width: 100vw;
            /* background-size: cover; */
            /* Ensure the background covers the entire container */
            /* background-position: center; */
            /* Center the background image */
            /* background-repeat: no-repeat; */
            /* Do not repeat the background image */
        }

        .hero-section .wave {
            width: 100%;
            overflow: hidden;
            position: absolute;
            z-index: 1;
            bottom: -150px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .wave {
                bottom: -180px;
            }
        }

        .hero-section .wave svg {
            width: 100%;
        }

        .hero-section,
        .hero-section>.container>.row {
            height: 100vh;
            min-height: 880px;
        }

        .hero-section.inner-page {
            height: 60vh;
            min-height: 0;
        }

        .hero-section.inner-page .hero-text {
            transform: translateY(-150px);
            margin-top: -120px;
        }

        @media screen and (max-width: 992px) {
            .hero-section.inner-page .hero-text {
                margin-top: -80px;
            }
        }

        .hero-section h1 {
            font-size: 3.5rem;
            color: #fff;
            font-weight: 700;
            margin-bottom: 30px;
        }

        @media screen and (max-width: 992px) {
            .hero-section h1 {
                font-size: 2.5rem;
                text-align: center;
                margin-top: 40px;
            }
        }

        @media screen and (max-width: 992px) {
            .hero-section .hero-text-image {
                margin-top: 4rem;
            }
        }

        .hero-section p {
            font-size: 18px;
            color: #fff;
        }

        .hero-section .iphone-wrap {
            position: relative;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap {
                text-align: center;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            position: absolute;
            top: -50%;
            overflow: hidden;
            left: 0;
            /* box-shadow: 0 15px 50px 0 rgba(0, 0, 0, 0.3); */
            border-radius: 30px;
        }

        @media screen and (max-width: 992px) {

            .hero-section .iphone-wrap .phone-2,
            .hero-section .iphone-wrap .phone-1 {
                position: relative;
                top: 0;
                max-width: 100%;
            }
        }

        .hero-section .iphone-wrap .phone-2,
        .hero-section .iphone-wrap .phone-1 {
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-1 {
                margin-left: -150px;
            }
        }

        .hero-section .iphone-wrap .phone-2 {
            margin-top: 50px;
            margin-left: 100px;
            width: 250px;
        }

        @media screen and (max-width: 992px) {
            .hero-section .iphone-wrap .phone-2 {
                width: 250px;
                position: absolute;
                margin-top: 0px;
                margin-left: 100px;
            }
        }
    </style>
@endsection
@section('cabecera')
    @include('carousel')
@endsection
@section('content')
    <!-- ======= Home Section ======= -->
    <section class="section" style="margin-top: -50px;">
        <div class="container">

            <div class="mb-5 text-center row justify-content-center">
                <div class="col-md-5 verde-gallinero" data-aos="fade-up">
                    <h2 class="section-heading verde-gallinero" style=""><a class="verde-gallinero"
                            href="{{ url('compañia') }}">ENTRAR</a></h2>
                </div>
            </div>

            <div class="col-md-12" data-aos="fade-up" data-aos-delay="">
                <div class="text-center feature-1">
                    {{--  <div class="wrap-icon icon-1">
                            <i class="bi bi-people"></i>
                        </div>  --}}
                    <h2 class="mb-3">Agradecemos el material fotográfico otorgado por: </h2>
                    <h5 class="text-primary">
                        <p class="text-primary">Aarón Govea, Andrés Rivera González, Carlos Miravá,</p>
                        <p class="text-primary">Dennis Yulov, Enid Hernández, Luis Eriko,</p>
                        <p class="text-primary">Pili Pala, Paulina Chávez, Ramsés Cruguer Munguía Rosales.</p>
                    </h5>
                </div>
            </div>

            {{--  <div class="row">
                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="">
                        <div class="text-center feature-1">
                            <div class="wrap-icon icon-1">
                                <i class="bi bi-people"></i>
                            </div>
                            <h3 class="mb-3">Explore Your Team</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                        </div>
                    </div>
                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
                        <div class="text-center feature-1">
                            <div class="wrap-icon icon-1">
                                <i class="bi bi-brightness-high"></i>
                            </div>
                            <h3 class="mb-3">Digital Whiteboard</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                        </div>
                    </div>
                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
                        <div class="text-center feature-1">
                            <div class="wrap-icon icon-1">
                                <i class="bi bi-bar-chart"></i>
                            </div>
                            <h3 class="mb-3">Design To Development</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem, optio.</p>
                        </div>
                    </div>
                </div>  --}}

        </div>
    </section>

    {{--  <section class="section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4 me-auto">
                        <h2 class="mb-4">Seamlessly Communicate</h2>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur at
                            reprehenderit optio,
                            laudantium eius quod, eum maxime molestiae porro omnis. Dolores aspernatur delectus impedit
                            incidunt
                            dolore mollitia esse natus beatae.</p>
                        <p><a href="#" class="btn btn-primary">Download Now</a></p>
                    </div>
                    <div class="col-md-6" data-aos="fade-left">
                        <img src="assets/img/undraw_svg_2.svg" alt="Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="order-2 col-md-4 ms-auto">
                        <h2 class="mb-4">Gather Feedback</h2>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur at
                            reprehenderit optio,
                            laudantium eius quod, eum maxime molestiae porro omnis. Dolores aspernatur delectus impedit
                            incidunt
                            dolore mollitia esse natus beatae.</p>
                        <p><a href="#" class="btn btn-primary">Download Now</a></p>
                    </div>
                    <div class="col-md-6" data-aos="fade-right">
                        <img src="assets/img/undraw_svg_3.svg" alt="Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </section> --}}

    <!-- ======= Testimonials Section ======= -->
    {{--  <section class="section border-top border-bottom">
            <div class="container">
                <div class="mb-5 text-center row justify-content-center">
                    <div class="col-md-4">
                        <h2 class="section-heading">Agradecemos el material fotografico otorgado por:</h2>
                    </div>
                </div>
                <div class="text-center row justify-content-center">
                    <div class="col-md-7">

                        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <div class="text-center review">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>Excellent App!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img src="assets/img/person_1.jpg" alt="Image"
                                                class="mb-3 img-fluid rounded-circle">
                                            <span class="d-block">
                                                <span class="text-black">Jean Doe</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                                <div class="swiper-slide">
                                    <div class="text-center review">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>This App is easy to use!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img src="assets/img/person_2.jpg" alt="Image"
                                                class="mb-3 img-fluid rounded-circle">
                                            <span class="d-block">
                                                <span class="text-black">Johan Smith</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                                <div class="swiper-slide">
                                    <div class="text-center review">
                                        <p class="stars">
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill"></span>
                                            <span class="bi bi-star-fill muted"></span>
                                        </p>
                                        <h3>Awesome functionality!</h3>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea
                                                delectus pariatur, numquam
                                                aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae
                                                deleniti minus animi,
                                                provident voluptates consectetur maiores quos.</p>
                                        </blockquote>

                                        <p class="review-user">
                                            <img src="assets/img/person_3.jpg" alt="Image"
                                                class="mb-3 img-fluid rounded-circle">
                                            <span class="d-block">
                                                <span class="text-black">Jean Thunberg</span>, &mdash; App User
                                            </span>
                                        </p>

                                    </div>
                                </div><!-- End testimonial item -->

                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  --}}
    <!-- End Testimonials Section -->

    <!-- ======= CTA Section ======= -->
    {{--  <section class="section cta-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="mb-5 text-center col-md-6 me-auto text-md-start mb-md-0">
                        <h2>Starts Publishing Your Apps</h2>
                    </div>
                    <div class="text-center col-md-5 text-md-end">
                        <p><a href="#" class="btn d-inline-flex align-items-center"><i
                                    class="bx bxl-apple"></i><span>App store</span></a> <a href="#"
                                class="btn d-inline-flex align-items-center"><i
                                    class="bx bxl-play-store"></i><span>Google play</span></a></p>
                    </div>
                </div>
            </div>
        </section>
        <!-- End CTA Section -->  --}}
@endsection
@section('scripts')
    @parent

    </script>
@endsection
