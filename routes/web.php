<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarteleraController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('/compañia', 'compañia')->name('compañia');

Route::view('/espectaculos', 'espectaculos')->name('espectaculos');

Route::view('/contacto', 'contacto')->name('contacto');

Route::view('/prensa', 'prensa')->name('prensa');

Route::get('cartelera', [CarteleraController::class, 'index'])->name('cartelera');

Route::get('/login', function () {
    Route::redirect('/admin/login');
})->name('login');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        Route::redirect('/admin');
    })->name('dashboard');

});
